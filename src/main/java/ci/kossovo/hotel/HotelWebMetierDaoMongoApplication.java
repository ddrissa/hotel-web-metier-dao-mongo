package ci.kossovo.hotel;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import ci.kossovo.hotel.document.Employe;
import ci.kossovo.hotel.document.NomRole;
import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.repository.EmployeRepository;
import ci.kossovo.hotel.repository.RoleRepository;

@SpringBootApplication
@EnableJms
public class HotelWebMetierDaoMongoApplication implements CommandLineRunner {
	EmployeRepository employeRepository;
	RoleRepository roleRepository;
	

	

	
	public HotelWebMetierDaoMongoApplication(EmployeRepository employeRepository, RoleRepository roleRepository) {
		this.employeRepository = employeRepository;
		this.roleRepository = roleRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(HotelWebMetierDaoMongoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Employe emp1 = new Employe("Diarra", "Drissa", "CNI001", "Ed2ci", "DA", new Date(), "Responsable");
		Employe emp2 = new Employe("Trqore", "Ablo", "CNI002", "Ed2ci", "DA", new Date(), "Responsable");
		//employeRepository.save(emp1);
		if (!employeRepository.findByCni("CNI001").isPresent()) {
			employeRepository.save(emp1);
		}
		if (!employeRepository.findByCni("CNI002").isPresent()) {
			employeRepository.save(emp2);
		}
		
		if (!roleRepository.existsByNom(NomRole.ROLE_USER)) {
			roleRepository.save(new Role(NomRole.ROLE_USER));
		}
		
		if (!roleRepository.existsByNom(NomRole.ROLE_EMPLOYE)) {
			roleRepository.save(new Role(NomRole.ROLE_EMPLOYE));
		}
	
		if (!roleRepository.existsByNom(NomRole.ROLE_ADMIN)) {
			roleRepository.save(new Role(NomRole.ROLE_ADMIN));
		}
		if (!roleRepository.findByNom(NomRole.ROLE_GERANT.name()).isPresent()) {
			roleRepository.save(new Role(NomRole.ROLE_GERANT));
		}
		if (!roleRepository.findByNom(NomRole.ROLE_DBA.name()).isPresent()) {
			roleRepository.save(new Role(NomRole.ROLE_DBA));
		}
		
		/*
		 * System.out.println("@@@@@@@@ exist@@@@@@@@");
		 * System.out.println(roleRepository.existsByNom(NomRole.ROLE_USER));
		 */
	}
}
