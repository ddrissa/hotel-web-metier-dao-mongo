package ci.kossovo.hotel.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ci.kossovo.hotel.document.NomRole;
import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.payload.ApiResponse;
import ci.kossovo.hotel.payload.RoleRequest;
import ci.kossovo.hotel.repository.RoleRepository;
import ci.kossovo.hotel.repository.UserRepository;

@RestController
@RequestMapping("/api")
@Secured("ROLE_DBA")
public class RoleController {
	private RoleRepository roleRepository;
	private UserRepository userRepository;

	public RoleController(RoleRepository roleRepository, UserRepository userRepository) {
		this.roleRepository = roleRepository;
		this.userRepository= userRepository;
	}

	@PostMapping("/roles")
	public ResponseEntity<?> creerRole(@RequestBody RoleRequest roleRequest) {
		Role role = null;
		NomRole nr = NomRole.valueOf(roleRequest.getNom());
		if (nr != null) {
			role = new Role(nr);
		} else {
			return ResponseEntity.ok(new ApiResponse(false, "Ce role n'est defini"));
		}
		roleRepository.save(role);

		return ResponseEntity
				.ok(new ApiResponse(true, "Le role " + roleRequest.getNom() + " est enregistré avec succès"));
	}
	
	
	public ResponseEntity<?> gererRole() {
		return null;
		
	}

}
