package ci.kossovo.hotel.controller;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ci.kossovo.hotel.document.NomRole;
import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.exception.AppException;
import ci.kossovo.hotel.mail.UserJms;
import ci.kossovo.hotel.payload.ApiResponse;
import ci.kossovo.hotel.payload.JwtAuthenticationResponse;
import ci.kossovo.hotel.payload.LoginRequest;
import ci.kossovo.hotel.payload.SignUpRequest;
import ci.kossovo.hotel.payload.UserCurrent;
import ci.kossovo.hotel.repository.RoleRepository;
import ci.kossovo.hotel.repository.UserRepository;
import ci.kossovo.hotel.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	// proprietes
	private AuthenticationManager authenticationManager;
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private PasswordEncoder passwordEncoder;
	private JwtTokenProvider jwtTokenProvider;
	private JmsTemplate jmsTemplate;

// contructeur pour remplacer @Autowired
	public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
			RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider,
			JmsTemplate jmsTemplate) {
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
		this.jwtTokenProvider = jwtTokenProvider;
		this.jmsTemplate = jmsTemplate;
	}
// Le corps

	@PostMapping("/signin")
	public ResponseEntity<?> authentifierUser(@Valid @RequestBody LoginRequest loginRequest) {
		try {
			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
			SecurityContextHolder.getContext().setAuthentication(authentication);

			String jwt = jwtTokenProvider.generateToken(authentication);

			return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
		} catch (AuthenticationException e) {
			return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	// inscription

	@PostMapping("/inscrire")
	public ResponseEntity<?> enregistrerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		/*
		 * Employe emp = employeRepository.findByCni(signUpRequest.getCni())
		 * .orElseThrow(() -> new AppException("L'employe proprietaire n'existe pas"));
		 */

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "username existe deja!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ApiResponse(false, "Adresse email existe deja!"), HttpStatus.BAD_REQUEST);
		}

		// Defini User

		User user = new User(signUpRequest.getCni(), signUpRequest.getName(), signUpRequest.getUsername(),
				signUpRequest.getEmail(), signUpRequest.getPassword());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		// ajout role
		/*
		 * String ROLE_USER = NomRole.ROLE_USER.name(); Role r1 =
		 * roleRepository.findByNom(ROLE_USER) .orElseThrow(() -> new
		 * AppException("Ce role " + ROLE_USER + " n'est pas defini!")); roles.add(r1);
		 */
		// user.setRoles();

		String ROLE_EMPLOYE = NomRole.ROLE_EMPLOYE.name();
		Role r2 = roleRepository.findByNom(ROLE_EMPLOYE)
				.orElseThrow(() -> new AppException("Ce role " + ROLE_EMPLOYE + " n'est pas defini!"));
		// roles.add(r2);
		user.setRoles(Collections.singleton(r2));

		User resultat = userRepository.save(user);
		UserJms uc = new UserJms(resultat.getName(), resultat.getUsername(), resultat.getEmail(),
				signUpRequest.getPassword());

		jmsTemplate.convertAndSend("hotel", uc);

		URI emplacement = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{uaername}")
				.buildAndExpand(resultat.getUsername()).toUri();

		return ResponseEntity.created(emplacement).body(new ApiResponse(true, "Utilisateur enregistré avec succès"));
	}

}
