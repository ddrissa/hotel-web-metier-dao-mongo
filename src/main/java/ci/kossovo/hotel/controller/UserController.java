package ci.kossovo.hotel.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.document.UserDba;
import ci.kossovo.hotel.exception.AppException;
import ci.kossovo.hotel.exception.InvalidHotelException;
import ci.kossovo.hotel.exception.ResourceNotFoundException;
import ci.kossovo.hotel.metier.UserService;
import ci.kossovo.hotel.payload.ApiResponse;
import ci.kossovo.hotel.payload.UserCurrent;
import ci.kossovo.hotel.payload.UserIdentityAvailability;
import ci.kossovo.hotel.payload.UserProfile;
import ci.kossovo.hotel.payload.UserRequest;
import ci.kossovo.hotel.payload.UserSummary;
import ci.kossovo.hotel.security.CurrentUser;
import ci.kossovo.hotel.security.UserPrincipal;

@RestController
@RequestMapping("/api")
public class UserController {

	private UserService userService;
	private PasswordEncoder passwordEncoder;

	public UserController(UserService userService, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.passwordEncoder = passwordEncoder;
	}

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('EMPLOYE')")
	public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(),
				currentUser.getName());
		return userSummary;
	}

	@GetMapping("/user/checkUsenemeDisponible")
	public UserIdentityAvailability checkUsernameDisponible(@RequestParam(value = "username") String username) {
		Boolean dispo = !userService.findByUsername(username).isPresent();
		return new UserIdentityAvailability(dispo);
	}

	@GetMapping("/user/checkEmailDisponible")
	public UserIdentityAvailability checkEmailDisponible(@RequestParam(value = "email") String email) {
		Boolean dispo = !userService.findByEmail(email).isPresent();
		return new UserIdentityAvailability(dispo);
	}

	@GetMapping("/users/{username}")
	public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
		User user = userService.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
		return new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getCreatedAt());
	}

	@PutMapping("/users/dba")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public ResponseEntity<?> modifier(@Valid @RequestBody UserRequest ur) {
		// Aller chercher user dans la base.
		User user = userService.findById(ur.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", ur.getId()));

		user.setTitulaire(ur.getTitulaire());
		user.setAccountNonExpired(ur.isAccountNonExpired());
		user.setAccountNonLocked(ur.isAccountNonLocked());
		user.setEnabled(ur.isEnabled());
		user.setRoles(ur.getRoles());
		// A completer

		try {
			user = userService.modifier(user);
		} catch (InvalidHotelException e) {

			return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.ok(new ApiResponse(true, "User modifié est  " + user.getUsername()));

	}

	@GetMapping("/user/current/{username}")
	@PreAuthorize("#current.username == auhentication.principal.username")
	public UserCurrent getUserCurrent(@PathVariable(value = "username") String username) {
		User u = userService.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		return new UserCurrent(u.getId(), u.getName(), u.getUsername(), u.getEmail(), u.getPassword());
	}

	// Modification par user
	@PutMapping("/user")
	@PreAuthorize("#current.username == auhentication.principal.username")
	public ResponseEntity<?> modifieUser(@RequestBody UserCurrent current) {
		User user = userService.findById(current.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", current.getName()));

		user.setName(current.getName());
		user.setUsername(current.getUsername());
		user.setEmail(current.getEmail());
		user.setPassword(passwordEncoder.encode(current.getPassword()));
		User us = null;

		try {
			us = userService.creer(user);
		} catch (InvalidHotelException e) {
			return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.ok(new ApiResponse(true, "User modifié est  " + us.getUsername()));
		
	}

	@GetMapping("/users/dba")
	@PreAuthorize("hasRole('GERANT') OR hasRole('DBA')")
	public List<UserDba> getUsers() {

		return userService.findUserDba();

	}

	@GetMapping("/users/dba/{username}")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public UserRequest getUserDba(@PathVariable(value = "username") String username) {
		User u = userService.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		return new UserRequest(u.getId(), u.getTitulaire(), u.getName(), u.getUsername(), u.getEmail(), u.getRoles(),
				u.isAccountNonExpired(), u.isAccountNonLocked(), u.isCredentialsNonExpired(), u.isEnabled());
	}
	// Creer GetOser()
	// Annuker mot de passe et mette un defauud
	// suppression
}
