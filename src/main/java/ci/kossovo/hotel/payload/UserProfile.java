package ci.kossovo.hotel.payload;

import java.time.LocalDateTime;

public class UserProfile {
	private String id;
	private String username;
	private String name;
	private LocalDateTime joinedAt;
	// Ajouter les autres qu'on voir dans l'inface du profil utilisateur

	public UserProfile(String id, String username, String name, LocalDateTime joinedAt) {
		this.id = id;
		this.username = username;
		this.name = name;
		this.joinedAt = joinedAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getJoinedAt() {
		return joinedAt;
	}

	public void setJoinedAt(LocalDateTime joinedAt) {
		this.joinedAt = joinedAt;
	}

	

}
