package ci.kossovo.hotel.payload;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserIdentityAvailability {
private Boolean disponible;

public UserIdentityAvailability(Boolean disponible) {
	this.disponible = disponible;
}



}
