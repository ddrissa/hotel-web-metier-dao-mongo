package ci.kossovo.hotel.payload;

public class RoleRequest {
	private String nom;
	private String desc;

	public RoleRequest() {
		// TODO Auto-generated constructor stub
	}

	
	public RoleRequest(String nom) {
		this.nom = nom;
	}


	public RoleRequest(String nom, String desc) {
		this.nom = nom;
		this.desc = desc;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
