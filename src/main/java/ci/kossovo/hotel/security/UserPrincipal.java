package ci.kossovo.hotel.security;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ci.kossovo.hotel.document.User;

public class UserPrincipal implements UserDetails {

	private String id;
	private String name;
	private String username;
	
	@JsonIgnore
	private String cni;
	@JsonIgnore
	private String email;
	@JsonIgnore
	private String password;
	@JsonIgnore
	private boolean accountNonExpired;
	@JsonIgnore
	private boolean accountNonLocked;
	@JsonIgnore
	private boolean credentialsNonExpired;
	@JsonIgnore
	private boolean enabled;
	private Collection<? extends GrantedAuthority> authorities;

	

	public UserPrincipal(String id, String name, String username, String cni, String email, String password,
			boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.cni = cni;
		this.email = email;
		this.password = password;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
		this.authorities = authorities;
	}

	public static UserPrincipal create(User user) {
		  List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
          new SimpleGrantedAuthority(role.getNom().name())
  ).collect(Collectors.toList());

		return new UserPrincipal(user.getId(), user.getName(), user.getUsername(),user.getTitulaire(), user.getEmail(), user.getPassword(),
				user.isAccountNonExpired(), user.isAccountNonLocked(), user.isCredentialsNonExpired(), user.isEnabled(),
				authorities
				);

	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}
	
	

	

	public String getCni() {
		return cni;
	}

	public void setCni(String cni) {
		this.cni = cni;
	}

	@Override
	public String getPassword() {

		return password;
	}

	@Override
	public String getUsername() {

		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		
		return enabled;
	}

}
