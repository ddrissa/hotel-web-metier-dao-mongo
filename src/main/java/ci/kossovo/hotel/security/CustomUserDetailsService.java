package ci.kossovo.hotel.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	UserRepository userRepository;

	

	public CustomUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameOremail) throws UsernameNotFoundException {
		User user = userRepository.findByUsernameOrEmail(usernameOremail, usernameOremail)
				.orElseThrow(()->
				new UsernameNotFoundException("Utilisateur non trouvé avec nom d'utilisateur ou email: "+usernameOremail)
				);
		return UserPrincipal.create(user);
	}
	
	
	// Cette méthode est utilisée par JWTAuthenticationFilter
	@Transactional
	public UserDetails loadUserById(String id) {
		User user = userRepository.findById(id).orElseThrow(
				()-> new UsernameNotFoundException("Pas user avec Id: "+id)
		
				);
		return UserPrincipal.create(user);
		
	}

}
