package ci.kossovo.hotel.security;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;
	
	//Champs pour califier les roles
	private boolean isAdmin = false;
	private boolean isEmploye = false;
	private boolean isClient = false;
	private boolean isUser = false;
	private boolean isGerant = false;
	private boolean isDba = false;

	
	public String generateToken(Authentication authentication) {
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		userPrincipal.getAuthorities().forEach(r -> {
			if (r.getAuthority() == "ROLE_ADMIN") isAdmin = true;
			if (r.getAuthority() == "ROLE_EMPLOYE") isEmploye = true;
			if (r.getAuthority() == "ROLE_CLIENT") isClient = true;
			if (r.getAuthority() == "ROLE_GERANT") isGerant = true;
			if (r.getAuthority() == "ROLE_USER") isUser = true;
			if (r.getAuthority() == "ROLE_DBA") isDba = true;
		});
		Date now = new Date();
		Date dateExpiration = new Date(now.getTime() + jwtExpirationInMs);
		
		return Jwts.builder()
				.setSubject(userPrincipal.getId())
				.setIssuedAt(new Date())
				.setExpiration(dateExpiration)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.claim("Roles", userPrincipal.getAuthorities())
				.claim("isAdmin", isAdmin)
				.claim("isEmploye", isEmploye)
				.claim("isClient", isClient)
				.claim("isGerant", isGerant)
				.claim("isUser", isUser)
				.claim("isDba", isDba)
				.compact();
	}

	// Aller chercher user par son id pour actualiser user eb cas de modification
	public String getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

	public boolean validateToken(String authToken) {

		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (ExpiredJwtException e) {
			logger.error("Token JWT expiré");
		} catch (UnsupportedJwtException e) {
			logger.error("Token JWT non pris en charge");
		} catch (MalformedJwtException e) {
			logger.error("Token JWT invalide");
		} catch (SignatureException e) {
			logger.error("Signature JWT invalide");
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string est vide.");
		}
		return false;

	}
}
