package ci.kossovo.hotel.metier;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import ci.kossovo.hotel.document.Employe;
import ci.kossovo.hotel.document.User;

public interface IEmployeMetier extends IMetier<Employe, String> {
	
	List<Employe> trouverNomCompletParMc(String mc);

	Stream<Employe> findByStream();

	Optional<User> findMyUser(String cni);
	Optional<User> findByTitulaire(String code);

}
