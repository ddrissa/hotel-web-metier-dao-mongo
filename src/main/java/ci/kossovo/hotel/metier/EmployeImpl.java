package ci.kossovo.hotel.metier;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import ci.kossovo.hotel.document.Employe;
import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.repository.EmployeRepository;
import ci.kossovo.hotel.repository.UserRepository;

@Service
public class EmployeImpl implements IEmployeMetier {
	
	private EmployeRepository employeRepository;
	private UserRepository userRepository;

	

	public EmployeImpl(EmployeRepository employeRepository, UserRepository userRepository) {
		this.employeRepository = employeRepository;
		this.userRepository = userRepository;
	}

	@Override
	public Employe creer(Employe entity) {
		return employeRepository.save(entity);
	}

	@Override
	public List<Employe> creerAll(List<Employe> entity) {
		return employeRepository.saveAll(entity);
	}

	@Override
	public Employe modifier(Employe entity) {
		return employeRepository.save(entity);
	}

	@Override
	public Optional<Employe> findById(String id) {
		return employeRepository.findById(id);
	}

	@Override
	public List<Employe> findAll() {
		return employeRepository.findAll();
	}

	@Override
	public void spprimer(List<Employe> entities) {
		employeRepository.deleteAll(entities);
	}

	@Override
	public boolean supprimer(String id) {
		employeRepository.deleteById(id);
		return true;
	}

	@Override
	public boolean existe(String id) {
		return employeRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return employeRepository.count();
	}

	@Override
	public List<Employe> trouverNomCompletParMc(String mc) {
		return employeRepository.findByNomCompletContainingIgnoreCase(mc);
	}

	@Override
	public Stream<Employe> findByStream() {
		return employeRepository.findBy();
	}

	@Override
	public Optional<User> findMyUser(String cni) {
		return userRepository.findByTitulaire(cni);
	}

	@Override
	public Optional<User> findByTitulaire(String code) {
		return userRepository.findByTitulaire(code);
	}

	

}
