package ci.kossovo.hotel.metier;

import java.util.List;
import java.util.Optional;

import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.document.UserDba;

public interface IUserMetier extends IMetier<User, String> {

	Optional<User> findByUsernameOrEmail(String username, String email);

	List<User> findByIdIn(List<String> userIds);

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);

	Optional<User> findByTitulaire(String code);

	List<UserDba> findUserDba();

	List<User> findByRoles(Role role);

}
