package ci.kossovo.hotel.metier;

import java.util.List;
import java.util.Optional;

import ci.kossovo.hotel.exception.InvalidHotelException;

public interface IMetier<T,U>{
	
	public T creer(T entity) throws InvalidHotelException;
	public List<T> creerAll(List<T> entity) throws InvalidHotelException;

	public T modifier(T entity) throws InvalidHotelException;

	public Optional<T> findById(U id);

	public List<T> findAll();

	public void spprimer(List<T> entities);

	public boolean supprimer(U id) throws InvalidHotelException;

	public boolean existe(U id);

	public Long compter();
	
	
}
