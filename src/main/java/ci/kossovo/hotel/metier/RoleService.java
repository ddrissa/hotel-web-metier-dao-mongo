package ci.kossovo.hotel.metier;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.exception.InvalidHotelException;
import ci.kossovo.hotel.repository.RoleRepository;
import ci.kossovo.hotel.repository.UserRepository;

public class RoleService implements IRoleMetier {

	private RoleRepository roleRepository;
	private UserRepository userRepository;

	public RoleService(RoleRepository roleRepository, UserRepository userRepository) {
		this.roleRepository = roleRepository;
		this.userRepository = userRepository;
	}

	@Override
	public Role creer(Role entity) throws InvalidHotelException {
		return roleRepository.save(entity);
	}

	@Override
	public List<Role> creerAll(List<Role> entities) throws InvalidHotelException {
		return roleRepository.saveAll(entities);
	}

	@Override
	public Role modifier(Role entity) throws InvalidHotelException {
		return roleRepository.save(entity);
	}

	@Override
	public Optional<Role> findById(String id) {
		return roleRepository.findById(id);
	}

	
	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}
	

	@Transactional
	@Override
	public void spprimer(List<Role> entities) {
entities.stream().forEach(r -> {
	if (roleRepository.existsById(r.getId())) {
		Role r1 = roleRepository.findById(r.getId()).get();
		if (supprimeUserRoles(r1)) {
			roleRepository.deleteById(r1.getId());
			
		}

	}
});
	}

	@Transactional
	@Override
	public boolean supprimer(String id) throws InvalidHotelException {
		if (roleRepository.existsById(id)) {
			Role r = roleRepository.findById(id).get();
			if (supprimeUserRoles(r)) {
				roleRepository.deleteById(id);
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}
	

	@Override
	public boolean existe(String id) {
		return roleRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return roleRepository.count();
	}

	@Override
	public Optional<Role> findByNom(String nom) {
		return roleRepository.findByNom(nom);
	}

	
	private boolean supprimeUserRoles(Role role) {
		List<User> users = userRepository.findByRoles(role);
		if (users.isEmpty()) {
			return true;
		}

		users.stream().forEach(u -> {
			Set<Role> rs = new HashSet<Role>();
			rs = u.getRoles();
			rs.remove(role);
			u.setRoles(rs);
			userRepository.save(u);

		});
		return true;

	}

}
