package ci.kossovo.hotel.metier;

import java.util.Optional;

import ci.kossovo.hotel.document.Role;

public interface IRoleMetier extends IMetier<Role, String> {
	Optional<Role> findByNom(String nom);
}
