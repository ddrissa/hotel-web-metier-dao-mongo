package ci.kossovo.hotel.metier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.document.User;
import ci.kossovo.hotel.document.UserDba;
import ci.kossovo.hotel.exception.InvalidHotelException;
import ci.kossovo.hotel.repository.UserRepository;

@Service
public class UserService implements IUserMetier {

	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Override
	public User creer(User entity) throws InvalidHotelException {
		return userRepository.save(entity);
	}
	
	@Override
	public List<User> creerAll(List<User> entity) throws InvalidHotelException{
		return userRepository.saveAll(entity);
	}

	@Override
	public User modifier(User entity) throws InvalidHotelException{
		return userRepository.save(entity);
	}

	@Override
	public Optional<User> findById(String id) {
		return userRepository.findById(id);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public void spprimer(List<User> entities) {
		userRepository.deleteAll(entities);
	}

	@Override
	public boolean supprimer(String id) throws InvalidHotelException{
		return userRepository.existsById(id);
	}

	@Override
	public boolean existe(String id) {
		return userRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return userRepository.count();
	}

	@Override
	public Optional<User> findByUsernameOrEmail(String username, String email) {
		return userRepository.findByUsernameOrEmail(username, email);
	}

	@Override
	public List<User> findByIdIn(List<String> userIds) {
		return userRepository.findByIdIn(userIds);
	}

	@Override
	public Optional<User> findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Optional<User> findByTitulaire(String code) {
		return userRepository.findByTitulaire(code);
	}

	@Override
	public List<UserDba> findUserDba() {
		List<UserDba> userdbs = new ArrayList<UserDba>();
		userRepository.findBy().map(
				u -> userdbs.add(new UserDba(u.getId(), u.getTitulaire(), u.getName(), u.getUsername(), u.getEmail())));

		return userdbs;
	}


	@Override
	public List<User> findByRoles(Role role) {
		return userRepository.findByRoles(role);
	}


	

}
