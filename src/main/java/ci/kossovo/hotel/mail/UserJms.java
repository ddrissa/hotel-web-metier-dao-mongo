package ci.kossovo.hotel.mail;

public class UserJms {
	private String name;
	private String usename;
	private String mail;
	private String password;
	
	
	public UserJms(String name, String usename, String mail, String password) {
		this.name = name;
		this.usename = usename;
		this.mail = mail;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsename() {
		return usename;
	}

	public void setUsename(String usename) {
		this.usename = usename;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserJms [name=" + name + ", usename=" + usename + ", mail=" + mail + ", password=" + password + "]";
	}

}
