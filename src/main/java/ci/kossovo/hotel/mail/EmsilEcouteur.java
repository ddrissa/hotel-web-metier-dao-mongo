package ci.kossovo.hotel.mail;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmsilEcouteur {
	private JavaMailSender javaMailSender;

	public EmsilEcouteur(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	@JmsListener(destination = "hotel", containerFactory="myFactory")
	public void envoyerMail(UserJms user) {
		if (user != null) {
			System.out.println("VOIR :" + user.toString());
			System.out.println("Composer de: ");
			System.out.println(user.getName());
			System.out.println(user.getUsename());
			System.out.println(user.getMail());
			System.out.println(user.getPassword());

			SimpleMailMessage message = new SimpleMailMessage();

			message.setTo(user.getMail());
			message.setText("Hotel app");
			message.setText("Bonjour Mme/Mr " + user.getName() + ". Vous venez d'etre inscrit au compte: Login: "
					+ user.getUsename() + " Password: " + user.getPassword());

			javaMailSender.send(message);
		}
	}
}
