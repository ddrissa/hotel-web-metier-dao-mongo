package ci.kossovo.hotel.document;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import ci.kossovo.hotel.document.audit.UserDateAudit;

@Document
public class User extends UserDateAudit {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@NotBlank(message = "La personne consernee ne peut etre nul ou vide.")
	private String titulaire;

	@NotBlank(message = "Le name est obligatoire")
	private String name;

	@NotBlank(message = "L'username est obligatoire")
	@Indexed(unique = true)
	private String username;

	@NotBlank(message = "L'email est obligatoire")
	@Email
	@Indexed(unique = true)
	private String email;
	@NotBlank(message = "Le password est obligatoire")
	@Size(min = 4)
	private String password;

	@DBRef
	private Set<Role> roles = new HashSet<>();

	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getId() {
		return id;
	}

	public User() {
		super();
	}

	public User(@NotBlank(message = "La personne consernee ne peut etre nul ou vide.") String cni,
			@NotBlank(message = "Le name est obligatoire") String name,
			@NotBlank(message = "L'username est obligatoire") String username,
			@NotBlank(message = "L'email est obligatoire") @Email String email,
			@NotBlank(message = "Le password est obligatoire") @Size(min = 4) String password) {
		this.titulaire = cni;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
	}

}
