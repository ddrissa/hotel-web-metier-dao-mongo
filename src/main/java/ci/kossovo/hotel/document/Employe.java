package ci.kossovo.hotel.document;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
@Getter @Setter
public class Employe extends Personne {
	private Date embauche;
	private String photo;
	private String fonction;
	

	public Employe() {
		super();

	}

	public Employe(@NotBlank(message = "Le nom ne peut etre nul ou vide") String nom,
			@NotBlank(message = "Le nom ne peut etre nul ou vide") String prenom, @NotBlank String numCni,
			String societe, String profession, Date embauche,  String fonction) {
		super(nom, prenom, numCni, societe, profession);
		this.embauche = embauche;
		this.fonction = fonction;
	}

}
