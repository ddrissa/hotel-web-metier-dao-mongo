package ci.kossovo.hotel.document;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserDba {
	private String id;
	private String titulaire;
	private String name;
	private String username;
	private String email;

	public UserDba() {
	}

	public UserDba(String id, String titulaire, String name, String username, String email) {
		this.id = id;
		this.titulaire = titulaire;
		this.name = name;
		this.username = username;
		this.email = email;
	}

	

}
