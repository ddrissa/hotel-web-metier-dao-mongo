package ci.kossovo.hotel.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Personne implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	@NotBlank(message = "Le nom ne peut etre nul ou vide")
	private String nom;
	@NotBlank(message = "Le nom ne peut etre nul ou vide")
	private String prenom;
	@NotBlank
	@Indexed(unique = true)
	private String cni;
	private String societe;
	private String profession;
	private Adresse adresse;
	private List<Telephone> telephones;
	private String nomComplet;

	public Personne() {
		super();
		this.telephones = new ArrayList<>();

	}

	public Personne(@NotBlank(message = "Le nom ne peut etre nul ou vide") String nom,
			@NotBlank(message = "Le nom ne peut etre nul ou vide") String prenom, @NotBlank String cni,
			String societe, String profession) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.cni = cni;
		this.societe = societe;
		this.profession = profession;
	}

	public Personne(@NotBlank(message = "Le nom ne peut etre nul ou vide") String nom,
			@NotBlank(message = "Le nom ne peut etre nul ou vide") String prenom, @NotBlank String cni,
			String societe, String profession, Adresse adresse, List<Telephone> telephones) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.cni = cni;
		this.societe = societe;
		this.profession = profession;
		this.adresse = adresse;
		this.telephones = telephones;
	}

}
