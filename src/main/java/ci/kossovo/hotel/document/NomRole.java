package ci.kossovo.hotel.document;

public enum NomRole {
	ROLE_USER,
    ROLE_ADMIN,
    ROLE_EMPLOYE,
    ROLE_CLIENT,
    ROLE_GERANT,
    ROLE_DBA

}
