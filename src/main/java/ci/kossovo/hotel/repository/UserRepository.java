package ci.kossovo.hotel.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.document.Role;
import ci.kossovo.hotel.document.User;

public interface UserRepository extends MongoRepository<User, String> {

	Stream<User> findBy();

	Optional<User> findByUsernameOrEmail(String username, String email);

	List<User> findByIdIn(List<String> userIds);

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String username);

	Optional<User> findByTitulaire(String code);

	List<User> findByRoles(Role role);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
