package ci.kossovo.hotel.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.document.NomRole;
import ci.kossovo.hotel.document.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
	
	Optional<Role> findByNom(String nom);
	Boolean existsByNom(NomRole nomRole);
	Boolean existsByNom(String nom);

}
